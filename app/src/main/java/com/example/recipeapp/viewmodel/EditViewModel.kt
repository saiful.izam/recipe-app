package com.example.recipeapp.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.recipeapp.model.RecipeDatabase
import com.example.recipeapp.model.RecipeModel
import kotlinx.coroutines.launch

class EditViewModel(application: Application) : BaseViewModel(application) {

    val recipeLiveData = MutableLiveData<RecipeModel>()

    fun saveNewRecipe(newRecipe: RecipeModel) {
        launch {
            val dao = RecipeDatabase(getApplication()).recipeDao()

            if(newRecipe.uuid == 0)
                dao.insertAll(newRecipe)
            else {
                dao.deleteRecipe(newRecipe.uuid)
                dao.insertAll(newRecipe)
            }
        }
    }

    fun fetch(uuid: Int) {

        launch {
            val dao = RecipeDatabase(getApplication()).recipeDao()

            val result = dao.getRecipe(uuid)

            recipeLiveData.value = result
        }
    }
}