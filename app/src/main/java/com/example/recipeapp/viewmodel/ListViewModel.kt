package com.example.recipeapp.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.recipeapp.R
import com.example.recipeapp.model.RecipeDatabase
import com.example.recipeapp.model.RecipeModel
import kotlinx.coroutines.launch

class ListViewModel(application: Application): BaseViewModel(application) {

    val recipes = MutableLiveData<List<RecipeModel>>()

    fun fetchData() {
        val recipe1 = RecipeModel("1", "Chicken Curry", "450 g\n" +
                "chicken meat\n" +
                "3 1⁄2 tbsp\n" +
                "curry powder \n" +
                "1 tbsp\n" +
                "light soy sauce \n" +
                "5\n" +
                "chilli peppers (dried) \n" +
                "3\n" +
                "candlenut(s) \n" +
                "3\n" +
                "garlic clove(s)\n" +
                "3\n" +
                "shallot(s) \n" +
                "2 cm\n" +
                "ginger\n" +
                "3 tbsp\n" +
                "oil\n" +
                "300 ml\n" +
                "water\n" +
                "2\n" +
                "potatoes\n" +
                "1\n" +
                "lemongrass stalk(s) \n" +
                "1\n" +
                "yellow onion(s) \n" +
                "5\n" +
                "curry leaves stalk(s) \n" +
                "300 ml\n" +
                "coconut milk\n" +
                "salt\n" +
                "sugar", "Lovely chicken curry!!"
            , "Chicken", "1. Marinate chicken meat with curry powder and soy sauce for 15 minutes.\n" +
                    "2. Soak dried chillies until soft and remove the membranes to reduce the spiciness. Blend candlenuts, garlic, shallots, ginger and dried chillies with curry powder.\n" +
                    "3. Heat oil in a pot over medium heat. Add blended chilli paste and cook until fragrant.\n" +
                    "4. Add chicken meat and mix well. Cook until the dish is slightly dry. Add water and cook over high heat.\n" +
                    "5. Add potatoes, onions, lemongrass and curry leaves. Once it boils, lower the heat and allow to simmer for 30 to 40 minutes.\n" +
                    "6. Add coconut milk and cook at high heat again. Once it boils, lower the heat. Add salt and sugar to taste.\n" +
                    "7. Serve with bread or rice.", "${R.drawable.chicken_curry}")

        val recipe2 = RecipeModel("2", "Sambal Chicken", "10 Shallot bulbs\n" +
                "6 Garlic cloves\n" +
                "2 Inches of Ginger\n" +
                "2 stalks of Lemongrass\n" +
                "2 Candle nuts (optional)\n" +
                "1 large Yellow Onion\n" +
                "1 Tomato\n" +
                "1 tablespoon of thick Coconut Milk\n" +
                "1 tablespoon of Black Sugar / Brown Sugar (optional)\n" +
                "1 whole Chicken (in parts)\n" +
                " 1 tablespoon of Turmeric Powder\n" +
                "Pepper\n" +
                "Salt\n" +
                "Oil\n" +
                "25-30 dry Red Chilies\n" +
                "Screw Pine leaves\n" +
                "150ml of Water", "Chicken sambal is a Malaysian favourite",
            "Chicken", "1. Clean the chicken parts and season it with salt, pepper and turmeric powder. Leave to marinate for 20-30 minutes.\\n\" +\n" +
                    "                    \"2. While the chicken marinates, blend shallots, garlic, ginger, lemongrass and candle nuts finely. Set aside.\\n\" +\n" +
                    "                    \"3. Prepare the dry red chilies by cutting it with scissors and boil in water for approximately 3-5 minutes. Drained water and give a quick  water wash to avoid bitter taste. Blend chilies as fine as possible.\\n\" +\n" +
                    "                    \"4. Slice yellow onion and dice tomato. Set it aside too.\\n\" +\n" +
                    "                    \"5. Heat enough oil to fry the marinated chicken to semi-fried. Set it aside for later to be mixed with the sambal sauce.\\n\" +\n" +
                    "                    \"6. After frying the chicken, use the same oil (reduce oil quantity to suitable amount to cook the sambal) and add in the finely blended ingredients. Stir until the colour start to turn brown & fragrant.\\n\" +\n" +
                    "                    \"7. Add in the blended dried red chili paste and let it cook till the oil afloat, then add in diced tomatoes. Pour in the 150 ml of water.\\n\" +\n" +
                    "                    \"8.Once tomato is soft, add in sliced onions and Screw pine leaves.\\n\" +\n" +
                    "                    \"9. After 5 minutes, add semi-fried chicken into the pan and let it simmer with the sambal for a few minutes until the chicken is cooked thoroughly.\\n\" +\n" +
                    "                    \"10. Stir in with coconut milk and let it further simmer for another 5 minutes.\\n\" +\n" +
                    "                    \"11.Sugar can be added for a sweeter sambal. (optional)\\n\" +\n" +
                    "                    \"12. Voila! Dish is ready to be served.", "${R.drawable.sambal_chicken}")

        val recipe3 = RecipeModel("3", "Chicken Soup", "(1) 5-6 pound whole roasting chicken (fresh or thawed)\n" +
                "Salt\n" +
                "Pepper\n" +
                "2 Tablespoons soft ghee (grass-fed butter, olive oil or vegan butter would work as well)\n" +
                "1 yellow onion, quartered\n" +
                "1 stalk celery, cut into 4–5 pieces\n" +
                "2 medium carrots, cut into 4 pieces each\n" +
                "1 head of garlic, cut of the top (or smash approx. 5–6 cloves)\n" +
                "1 sprig fresh rosemary\n" +
                "3–4 sprigs fresh thyme\n" +
                "1–2 fresh sage leaves\n" +
                "1 bay leaf\n" +
                "1–2 fresh sprig oregano\n" +
                "1 Tablespoon black peppercorn", "this is best chicken soup",
            "Chicken", "1.Preheat oven to 375.\n" +
                    "2.Place chicken in 5 1/2 qt. Le Creuset Dutch Oven and rub all over with softened ghee\n" +
                    "3.Sprinkle with generous amounts of salt and pepper to coat.\n" +
                    "4.Place onion, carrots, celery and garlic around the chicken.\n" +
                    "5.Roast, uncovered, for 1.5 hours.\n" +
                    "6.Remove from oven and place on burner.\n" +
                    "7.Add broth (and/or water) and turn heat up to high.\n" +
                    "8.Add rosemary, thyme, sage, bay leaf, oregano, peppercorn.\n" +
                    "9.Bring to a boil and then reduce to a simmer and cover.\n" +
                    "10.Simmer (covered) as long as possible – if you used chicken broth I would suggest 2 hours.", "${R.drawable.chicken_soup}")

        val recipe4 = RecipeModel("", "Mutton Curry", "2 pounds goat or sheep leg (cut into suitable bite-sized chunks)\n" +
                "4 tablespoons vegetable oil (or canola or sunflower cooking oil)\n" +
                "2 large onions (sliced thin)\n" +
                "2 large tomatoes (diced)\n" +
                "2 tablespoons garlic paste\n" +
                "1 tablespoon ginger paste\n" +
                "2 teaspoons coriander powder\n" +
                "1 teaspoon cumin powder\n" +
                "1/2 teaspoon turmeric powder\n" +
                "1/2 teaspoon red chili powder\n" +
                "2 teaspoons garam masala powder\n" +
                "Salt (to taste)\n" +
                "Garnish: coriander (chopped)", "The best Mutton Curry", "Meat", "1. Gather the ingredients.\n" +
                "2. Heat the cooking oil in a heavy bottomed pan, on medium heat.\n" +
                "3. When hot, add the onions. Sauté until the onions begin to turn a pale golden brown. Now remove from the oil with a slotted spoon and drain on paper towels. Turn off the heat.\n" +
                "4. Grind the onions into a smooth paste (adding very little to no water) in a food processor. Once done, move into a separate container.\n" +
                "5. Now grind the tomatoes, garlic, and ginger paste together in the food processor, into a smooth paste. Remove into a separate container and keep aside for later use.\n" +
                "6. Heat the oil left over from frying the onion and add the onion paste. Sauté for 2 to 3 minutes.\n" +
                "7. Now add the tomato paste and all the powdered spices, including the garam masala. Mix well.\n" +
                "8. Sauté the resulting masala (onion-tomato-spice mixture) until the oil begins to separate from it. This can take up to 10 minutes to happen.\n" +
                "9. Next, add the goat/mutton pieces to the masala, season with salt to taste and stir to fully coat the goat/mutton pieces with the masala. Sauté until the goat/mutton is browned well.\n" +
                "10. Add 1/2 a cup of hot water to the pan, stir to mix well, simmer the heat, and cover the pan. Cook till the goat/mutton is tender. You will need to keep checking on the goat/mutton as it cooks and adding more water if all the water dries up. Stir often to prevent burning. The dish should have a fairly thick gravy when done.\n" +
                "11.When the meat is cooked, garnish with chopped coriander and serve with hot chapatis (Indian flatbread), naan (tandoor-baked Indian flatbread), or plain boiled rice.", "${R.drawable.mutton_curry}")

        val recipe5 = RecipeModel("", "Mutton Soup", "10 Shallot bulbs\n" +
                "6 Garlic cloves\n" +
                "2 Inches of Ginger\n" +
                "2 stalks of Lemongrass\n" +
                "2 Candle nuts (optional)\n" +
                "1 large Yellow Onion\n" +
                "1 Tomato\n" +
                "1 tablespoon of thick Coconut Milk\n" +
                "1 tablespoon of Black Sugar / Brown Sugar (optional)\n" +
                "1 whole Chicken (in parts)\n" +
                " 1 tablespoon of Turmeric Powder\n" +
                "Pepper\n" +
                "Salt\n" +
                "Oil\n" +
                "25-30 dry Red Chilies\n" +
                "Screw Pine leaves\n" +
                "150ml of Water", "Best Mutton Soup!!", "Meat", "1. Clean the chicken parts and season it with salt, pepper and turmeric powder. Leave to marinate for 20-30 minutes.\n" +
                "2. While the chicken marinates, blend shallots, garlic, ginger, lemongrass and candle nuts finely. Set aside.\n" +
                "3. Prepare the dry red chilies by cutting it with scissors and boil in water for approximately 3-5 minutes. Drained water and give a quick  water wash to avoid bitter taste. Blend chilies as fine as possible.\n" +
                "4. Slice yellow onion and dice tomato. Set it aside too.\n" +
                "5. Heat enough oil to fry the marinated chicken to semi-fried. Set it aside for later to be mixed with the sambal sauce.\n" +
                "6. After frying the chicken, use the same oil (reduce oil quantity to suitable amount to cook the sambal) and add in the finely blended ingredients. Stir until the colour start to turn brown & fragrant.\n" +
                "7. Add in the blended dried red chili paste and let it cook till the oil afloat, then add in diced tomatoes. Pour in the 150 ml of water.\n" +
                "8.Once tomato is soft, add in sliced onions and Screw pine leaves.\n" +
                "9. After 5 minutes, add semi-fried chicken into the pan and let it simmer with the sambal for a few minutes until the chicken is cooked thoroughly.\n" +
                "10. Stir in with coconut milk and let it further simmer for another 5 minutes.\n" +
                "11.Sugar can be added for a sweeter sambal. (optional)\n" +
                "12. Voila! Dish is ready to be served.", "${R.drawable.mutton_soup}")

        val recipe6 = RecipeModel("", "Fish Curry", "450 g\n" +
                "chicken meat\n" +
                "3 1⁄2 tbsp\n" +
                "curry powder \n" +
                "1 tbsp\n" +
                "light soy sauce \n" +
                "5\n" +
                "chilli peppers (dried) \n" +
                "3\n" +
                "candlenut(s) \n" +
                "3\n" +
                "garlic clove(s)\n" +
                "3\n" +
                "shallot(s) \n" +
                "2 cm\n" +
                "ginger\n" +
                "3 tbsp\n" +
                "oil\n" +
                "300 ml\n" +
                "water\n" +
                "2\n" +
                "potatoes\n" +
                "1\n" +
                "lemongrass stalk(s) \n" +
                "1\n" +
                "yellow onion(s) \n" +
                "5\n" +
                "curry leaves stalk(s) \n" +
                "300 ml\n" +
                "coconut milk\n" +
                "salt\n" +
                "sugar", "Best fish curry!", "Fish", "1. Marinate chicken meat with curry powder and soy sauce for 15 minutes.\n" +
                "2. Soak dried chillies until soft and remove the membranes to reduce the spiciness. Blend candlenuts, garlic, shallots, ginger and dried chillies with curry powder.\n" +
                "3. Heat oil in a pot over medium heat. Add blended chilli paste and cook until fragrant.\n" +
                "4. Add chicken meat and mix well. Cook until the dish is slightly dry. Add water and cook over high heat.\n" +
                "5. Add potatoes, onions, lemongrass and curry leaves. Once it boils, lower the heat and allow to simmer for 30 to 40 minutes.\n" +
                "6. Add coconut milk and cook at high heat again. Once it boils, lower the heat. Add salt and sugar to taste.\n" +
                "7. Serve with bread or rice.", "${R.drawable.fish_curry}")

        val recipeList = arrayListOf(recipe1, recipe2, recipe3, recipe4, recipe5, recipe6)

        storeRecipe(recipeList)
    }

    fun retrieveFromLocal() {
        launch {
            val dao = RecipeDatabase(getApplication()).recipeDao()
            val result = dao.getAllRecipes()

            recipes.value = result
        }
    }

    fun filterRecipe(type: String) {
        launch {
            val dao = RecipeDatabase(getApplication()).recipeDao()

            recipes.value = dao.getRecipeByType(type)
        }
    }

    fun deleteRecipe(uuid: Int){
        launch {
            val dao = RecipeDatabase(getApplication()).recipeDao()

            dao.deleteRecipe(uuid)
        }
    }

    private fun storeRecipe(recipeList: List<RecipeModel>){
        launch {
           val dao = RecipeDatabase(getApplication()).recipeDao()

            dao.deleteAllRecipes()

            val result = dao.insertAll(*recipeList.toTypedArray())

            var i = 0

            while(i < recipeList.size){
                recipeList[i].uuid = result[i].toInt()
                ++i
            }

            recipes.value = recipeList
        }
    }

    override fun onCleared() {
        super.onCleared()
    }
}