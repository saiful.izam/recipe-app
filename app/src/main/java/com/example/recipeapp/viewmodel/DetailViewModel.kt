package com.example.recipeapp.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.recipeapp.model.RecipeDatabase
import com.example.recipeapp.model.RecipeModel
import kotlinx.coroutines.launch

class DetailViewModel(application: Application): BaseViewModel(application){
    val recipeLiveData = MutableLiveData<RecipeModel>()

    fun fetch(uuid: Int) {
        launch {
            val recipe = RecipeDatabase(getApplication()).recipeDao().getRecipe(uuid)
            recipeLiveData.value = recipe
        }
    }
}