package com.example.recipeapp.view

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.recipeapp.R
import com.example.recipeapp.model.RecipeModel
import com.example.recipeapp.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.item_recipe.view.*

class RecipeListAdapter(private val recipeList: ArrayList<RecipeModel>,
                        private val listener : OnItemClickListener)
    : RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder>() {

    inner class RecipeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView? = view.findViewById(R.id.title)
        val desc: TextView? = view.findViewById(R.id.description)
        val dishType: TextView? = view.findViewById(R.id.dishType)
    }

    interface OnItemClickListener {
        fun edit(position: Int)
        fun delete(uuid: Int)
        fun view(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_recipe, parent, false)

        return RecipeViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.title?.text = recipeList[position].recipeName
        holder.desc?.text = recipeList[position].recipeDescription
        holder.dishType?.text = recipeList[position].recipeType
        if(recipeList[position].recipeThumbnails != "")
            holder.itemView.ivRecipeImg.setImageResource(recipeList[position].recipeThumbnails!!.toInt())
        if(recipeList[position].isNewRecipe && recipeList[position].imgUri != null) {
            holder.itemView.ivRecipeImg.setImageURI(recipeList[position].imgUri!!.toUri())
        }

        holder.itemView.setOnClickListener {
            listener.view(position)
        }

        holder.itemView.deleteRecipe.setOnClickListener {
            listener.delete(recipeList[position].uuid)
        }

        holder.itemView.editRecipe.setOnClickListener {
            listener.edit(position)
        }
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }

    fun updateRecipeList(newRecipeList: List<RecipeModel>) {
        recipeList.clear()
        recipeList.addAll(newRecipeList)
        notifyDataSetChanged()
    }
}