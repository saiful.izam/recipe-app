package com.example.recipeapp.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.recipeapp.R
import com.example.recipeapp.model.RecipeModel
import com.example.recipeapp.viewmodel.EditViewModel
import kotlinx.android.synthetic.main.activity_recipe_edit.*

class RecipeEditActivity: AppCompatActivity() {

    companion object{
        private val IMAGE_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
    }
    private lateinit var viewmodel: EditViewModel

    private val recipeType = arrayOf("Chicken", "Meat", "Fish")
    private var recipeTypeSelected = ""
    private var uuid: Int = 0
    private var isEdit = false
    private var imgUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_recipe_edit)

        viewmodel = ViewModelProviders.of(this).get(EditViewModel::class.java)

        uuid = intent.extras?.get("uuid").toString().toInt()
        isEdit = intent.extras?.get("isEdit").toString().toBoolean()

        if(isEdit)
            btnAddRecipe.text = "Update Recipe"

        if(uuid != 0){
            viewmodel.fetch(uuid)
            observeViewModel()
        }

        imgPickBtn.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions, PERMISSION_CODE)
                }
                else{
                    pickImgFromGallery()
                }
            }else{
                pickImgFromGallery()
            }
        }

        typeSpinner.adapter = ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, recipeType)

        typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                recipeTypeSelected = recipeType[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        btnAddRecipe.setOnClickListener {
            fetchInput()
        }
    }

    private fun pickImgFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    private fun fetchInput() {
        val recipeInput = RecipeModel(
            "", editTitle.text.toString(),
            editIngredient.text.toString(),
            editDesc.text.toString(),
            recipeTypeSelected,
            editSteps.text.toString(), viewmodel.recipeLiveData.value?.recipeThumbnails, true, imgUri.toString()
        )

        recipeInput.uuid = uuid

        saveNewRecipe(recipeInput)
    }

    private fun saveNewRecipe(newRecipe: RecipeModel){
        viewmodel.saveNewRecipe(newRecipe)

        if(uuid != 0 )
            Toast.makeText(this, "Successfully update the recipe!", Toast.LENGTH_LONG).show()

        Toast.makeText(this, "Successfully added new recipe!", Toast.LENGTH_LONG).show()

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("isRefresh", true)

        startActivity(intent)
        finish()
    }

    private fun observeViewModel() {
        viewmodel.recipeLiveData.observe(this, Observer { recipe ->
            recipe?.let {
                editTitle.setText(recipe.recipeName)
                editDesc.setText(recipe.recipeDescription)
                typeSpinner.setSelection(recipeType.indexOf(recipe.recipeType))
                editIngredient.setText(recipe.recipeIngredients)
                editSteps.setText(recipe.recipeSteps)
                if(recipe.recipeThumbnails != "")
                    editImg.setImageResource(recipe.recipeThumbnails!!.toInt())
                if(recipe.isNewRecipe && recipe.imgUri != null)
                    editImg.setImageURI(recipe.imgUri!!.toUri())
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImgFromGallery()
                }else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            editImg.setImageURI(data?.data)
            imgUri = data?.data
        }
    }
}