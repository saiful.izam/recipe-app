package com.example.recipeapp.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipeapp.R
import com.example.recipeapp.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recipe.*

class MainActivity: AppCompatActivity(), RecipeListAdapter.OnItemClickListener{
    private lateinit var viewmodel: ListViewModel
    private val recipeListAdapter = RecipeListAdapter(arrayListOf(), this)

    private val recipeType = arrayOf("All","Chicken", "Meat", "Fish")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var isRefresh = intent.extras?.get("isRefresh").toString().toBoolean()
        viewmodel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        if(!isRefresh)
            viewmodel.fetchData()
        else
            viewmodel.retrieveFromLocal()

        rvRecipeList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = recipeListAdapter
        }

        observeViewModel()

        spinnerRecipe.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, recipeType)

        spinnerRecipe.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val type = recipeType[position]
                if(type == "All") {
                    viewmodel.retrieveFromLocal()
                    observeViewModel()
                }
                else {
                    viewmodel.filterRecipe(type)
                    observeViewModel()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

        fabAddRecipe.setOnClickListener {
            val intent = Intent(this, RecipeEditActivity::class.java)
            intent.putExtra("uuid", 0)
            intent.putExtra("isEdit", false)
            startActivity(intent)
            finish()
        }
    }

    private fun observeViewModel() {
        viewmodel.recipes.observe(this, Observer { recipeList ->
            recipeList?.let {
                recipeListAdapter.updateRecipeList(recipeList)
            }
        })
    }

    override fun edit(position: Int) {
        val recipe = viewmodel.recipes.value?.get(position)
        val intent = Intent(this, RecipeEditActivity::class.java)
            intent.putExtra("uuid", recipe?.uuid)
            intent.putExtra("isEdit", true)
            startActivity(intent)
    }

    override fun delete(uuid: Int) {
        viewmodel.deleteRecipe(uuid)
        viewmodel.retrieveFromLocal()
        observeViewModel()
    }

    override fun view(position: Int) {
        val recipe = viewmodel.recipes.value?.get(position)

        val intent = Intent(this, RecipeDetailActivity::class.java)
        intent.putExtra("uuid", recipe?.uuid)
        startActivity(intent)
    }
}