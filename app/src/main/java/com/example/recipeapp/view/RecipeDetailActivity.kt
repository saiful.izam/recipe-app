package com.example.recipeapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.recipeapp.R
import com.example.recipeapp.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.activity_recipe_detail.*

class RecipeDetailActivity: AppCompatActivity() {

    private lateinit var viewmodel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_recipe_detail)

        var uuid = intent.extras?.get("uuid").toString().toInt()

        viewmodel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        viewmodel.fetch(uuid)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewmodel.recipeLiveData.observe(this, Observer {recipe ->
            recipe?.let {
                recipeTitle.text = recipe.recipeName
                recipeDesc.text = recipe.recipeDescription
                recipeType.text = recipe.recipeType
                ingredients.text = recipe.recipeIngredients
                steps.text = recipe.recipeSteps

                if(recipe.recipeThumbnails != "")
                    recipeImg.setImageResource(recipe.recipeThumbnails!!.toInt())
                if(recipe.isNewRecipe && recipe.imgUri != null) {
                    recipeImg.setImageURI(recipe.imgUri!!.toUri())
                }
            }
        })

    }
}