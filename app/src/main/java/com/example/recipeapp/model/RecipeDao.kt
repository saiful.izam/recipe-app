package com.example.recipeapp.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RecipeDao {
    @Insert
    suspend fun insertAll(vararg recipes: RecipeModel) : List<Long>

    @Query("SELECT * FROM recipe")
    suspend fun getAllRecipes() : List<RecipeModel>

    @Query("SELECT * FROM recipe WHERE uuid= :recipeId")
    suspend fun getRecipe(recipeId: Int): RecipeModel

    @Query("DELETE FROM recipe")
    suspend fun deleteAllRecipes()

    @Query("DELETE FROM recipe WHERE uuid= :recipeId")
    suspend fun deleteRecipe(recipeId: Int)

    @Query("SELECT * FROM recipe WHERE recipe_type = :recipeType")
    suspend fun getRecipeByType(recipeType: String) : List<RecipeModel>

}