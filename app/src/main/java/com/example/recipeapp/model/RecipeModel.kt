package com.example.recipeapp.model

import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "recipe")
data class RecipeModel(
    @ColumnInfo(name = "recipe_id")
    @SerializedName(value = "recipe_id")
    var id: String?,

    @ColumnInfo(name = "recipe_name")
    @SerializedName(value = "recipe_name")
    var recipeName: String?,

    @ColumnInfo(name = "recipe_ingredient")
    @SerializedName(value = "recipe_ingredient")
    var recipeIngredients: String?,

    @ColumnInfo(name = "recipe_desc")
    @SerializedName(value = "recipe_desc")
    var recipeDescription: String?,

    @ColumnInfo(name = "recipe_type")
    @SerializedName(value = "recipe_type")
    var recipeType: String?,

    @ColumnInfo(name = "recipe_step")
    @SerializedName(value = "recipe_step")
    var recipeSteps: String?,

    @ColumnInfo(name = "recipe_img")
    @SerializedName(value = "recipe_img")
    var recipeThumbnails: String?,

    @ColumnInfo(name="is_new_recipe")
    @SerializedName(value = "is_new_recipe")
    var isNewRecipe: Boolean = false,

    @ColumnInfo(name = "img_uri")
    @SerializedName(value = "img_uri")
    var imgUri: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var uuid : Int = 0
}